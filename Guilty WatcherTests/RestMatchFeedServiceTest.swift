//
//  RestMatchFeedServiceTest.swift
//  Guilty WatcherTests
//
//  Created by Petr Bludsky on 26/12/2017.
//  Copyright © 2017 Petr Bludsky. All rights reserved.
//

import XCTest
import SwiftyJSON
@testable import Guilty_Watcher

/// RestMatchFeedServiceTest test suite
class RestMatchFeedServiceTest: XCTestCase {
    
    let service = RestMatchFeedService()
    
    /// Tests JSON parsing to match
    func testParseJson() {
        // Given
        let jsonString = "[{\"video\":\"KyrecqJe7DQ\",\"title\":\"高田馬場ミカド GUILTY GEAR Xrd REV2 10月7日 土曜初中級者大会＆固定3ON大会前配信配信\",\"channel\":\"じょにお\",\"date\":\"2017-10-07\",\"version\":\"R2\",\"timestamp\":\"01h21m27s\",\"players\":[{\"characters\":[{\"id\":\"dizzy\",\"name\":\"Dizzy\"}],\"name\":\"Ruki\",\"aliases\":[\"Ruki\",\"Rukkii\"]},{\"characters\":[{\"id\":\"answer\",\"name\":\"Answer\"}],\"name\":\"J.T\",\"aliases\":[\"hauko_mmts\",\"JT\",\"J.T\"]}],\"channelId\":\"UCDmFkuRZSbxyvqdK-cjMSog\"}]"
        let json = JSON.init(parseJSON: jsonString)
        
        // When
        let result = service.parseJson(json: json)
        
        // Then
        XCTAssert(result.count == 1)
        let match = result[0]
        XCTAssertEqual(match.videoHandle, "KyrecqJe7DQ")
        XCTAssertEqual(match.title, "高田馬場ミカド GUILTY GEAR Xrd REV2 10月7日 土曜初中級者大会＆固定3ON大会前配信配信")
        XCTAssertEqual(match.date, service.formatter.date(from: "2017-10-07"))
        XCTAssertEqual(match.firstCharacter.id, GameConfiguration.characters["dizzy"]!.id)
        XCTAssertEqual(match.secondCharacter.id, GameConfiguration.characters["answer"]!.id)
        XCTAssertEqual(match.firstPlayer, "Ruki")
        XCTAssertEqual(match.secondPlayer, "J.T")
        XCTAssertFalse(match.isWatched)
        XCTAssertFalse(match.isBookmarked)
    }
    
    /// Tests time string parsing to number of seconds
    func testParseTimestampToSeconds() {
        // Given
        let timeString = "03h42m20s"
        
        // When
        let result = service.parseTimestampToSeconds(timestamp: timeString)
        
        // Then
        XCTAssertEqual(result, 13340)
    }
    
    /// Tests regex search in a given string
    func testMatches() {
        // Given
        let regex = "[0-9]{2}"
        let string = "03h42m20s"
        
        // When
        let result = service.matches(for: regex, in: string)
        
        // Then
        XCTAssert(result.count == 3)
        XCTAssertEqual(result[0], "03")
        XCTAssertEqual(result[1], "42")
        XCTAssertEqual(result[2], "20")
    }
}
