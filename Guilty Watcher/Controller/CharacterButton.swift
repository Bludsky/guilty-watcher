//
//  CharacterButton.swift
//  Guilty Watcher
//
//  Created by Petr Bludsky on 19/12/2017.
//  Copyright © 2017 Petr Bludsky. All rights reserved.
//

import UIKit

/// Button representing character on the character select screen
class CharacterButton: UIButton {

    /// Character id set via ib's dynamic attributes
    @IBInspectable
    var characterId: String?
    
}
