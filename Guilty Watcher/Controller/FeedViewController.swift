//
//  ViewController.swift
//  Guilty Watcher
//
//  Created by Petr Bludsky on 30/10/2017.
//  Copyright © 2017 Petr Bludsky. All rights reserved.
//

import UIKit
import AVKit
import SVProgressHUD
import XCDYouTubeKit

/// Able to handle character select confirmation
protocol CharacterSelectConfirmable {
    
    /// Confirm character select (from modal window in this case)
    ///
    /// - Parameters:
    ///   - firstCharacter: first character
    ///   - secondCharacter: second character
    func confirmCharacterSelect(firstCharacter: Character, secondCharacter: Character)
    
    /// Prepare UI for bookmark mode deactivation
    func deactivateBookmarkMode()
    
    /// Prepare UI for character select deactivation
    func deactivateCharacterSelect()
}

/// Main VC presenting match feed and menu
class FeedViewController: OrientationAwareViewController {
    
    // MARK: Views
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var bottomMenuView: UIView!
    @IBOutlet weak var feedTableView: UITableView!
    
    // MARK: Buttons
    @IBOutlet weak var bookmarkButton: UIButton!
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var characterSelectButton: UIButton!
    
    /// Video player instance
    let playerViewController = AVPlayerViewController()
    
    /// Rest match feed service
    private let matchFeedService = RestMatchFeedService()
    
    /// Loaded matches
    var matches = [Match]()
    
    /// Current feed page
    var feedFilter: FeedFilter = FeedFilter()
    
    /// If true, more matches can be fetched
    var isMoreMatchesToFetch = true
    
    /// If true, matches are fetched from stored bookmarks
    var isBookmarkModeOn = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        
        // Bottom view panel needs to be transparent without affecting its children alpha channel
        bottomMenuView.backgroundColor = UIColor.clear
        
        // Reconfigure table height row in case a user changed device rotation in modal/other view
        configureTableRowHeight(screenHeight: Float(UIScreen.main.bounds.height))
        
        // Initial data fetch
        fetchMoreMatches()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        background = backgroundImageView
        super.viewDidAppear(animated)
        
        // Reconfigure table height row in case a user changed device rotation in modal/other view
        configureTableRowHeight(screenHeight: Float(UIScreen.main.bounds.height))
        
        // Reset selected styles of table cells (ui sugar)
        for cell in feedTableView.visibleCells {
            cell.setSelected(false, animated: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        SVProgressHUD.dismiss()
    }
    
    // Upate view background upon orientation transition detection
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        configureTableRowHeight(screenHeight: Float(size.height))
    }
    
    
    /// Calculate match table view row height based on screen height and bottom menu height ratio
    ///
    /// - Parameter screenHeight: screen height calculation is executed for
    private func configureTableRowHeight(screenHeight: Float) {
        feedTableView.estimatedRowHeight = CGFloat(screenHeight * Float(1 - UIConfiguration.bottomMenuHeightRatio) / 10)
    }
    
    /// Initial configuration of match table view
    private func configureTableView() {
        feedTableView.delegate = self
        feedTableView.dataSource = self
        
        // Register reusable cells
        feedTableView.register(UINib(nibName: "LeftMatchTableViewCell", bundle: nil), forCellReuseIdentifier: UIConfiguration.leftMatchTableCellViewReusableId)
        feedTableView.register(UINib(nibName: "RightMatchTableViewCell", bundle: nil), forCellReuseIdentifier: UIConfiguration.rightMatchTableCellViewReusableId)
        
        feedTableView.backgroundColor = .clear
        feedTableView.separatorColor = .clear
    }
    
    /// Refresh all vc data
    private func refresh() {
        deactivateBookmarkMode()
        deactivateCharacterSelect()
        resetFetchedData()
        fetchMoreMatches()
    }
    
    @IBAction func refreshButtonPressed(_ sender: UIButton) {
        // Also clear the filter
        feedFilter.reset()
        refresh()
    }
    
    @IBAction func bookmarkButtonPressed(_ sender: UIButton) {
        activateBookmarkMode()
        // Bookmark mode on = fetch matches from stored bookmarks
        let bookmarkedMatches = MatchCoreDataStorage.shared.fetchBookmarks()
        if bookmarkedMatches.isEmpty {
            // No bookmarks stored yet
            showAlert(title: "No bookmarks stored yet", message: "Swipe any match to the left to invoke bookmark menu")
            deactivateBookmarkMode()
            return
        }
        matches = bookmarkedMatches
        isMoreMatchesToFetch = false
        feedFilter.page = RestConfiguration.firstPageIndex
        feedTableView.reloadData()
        // Scroll to top
        let indexPath = IndexPath(row: 0, section: 0)
        feedTableView.scrollToRow(at: indexPath, at: .top, animated: false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToCharacterSelect" {
            // Character select modal window about to popup
            let characterSelectViewController = segue.destination as! CharacterSelectViewController
            characterSelectViewController.delegate = self
        }
    }
    
    /// Activate bookmark mode and update the UI
    private func activateBookmarkMode() {
        isBookmarkModeOn = true
        bookmarkButton.setImage(#imageLiteral(resourceName: "BookmarkActive.png"), for: .normal)
    }
    
    /// Update the UI for character select
    private func activateCharacterSelect() {
        characterSelectButton.setImage(#imageLiteral(resourceName: "CharacterSelectActive.png"), for: .normal)
    }
    
    @IBAction func characterSelectButtonPressed(_ sender: UIButton) {
        activateCharacterSelect()
    }
    
}

// Table view protocol implementation and match feed handling
extension FeedViewController: UITableViewDelegate, UITableViewDataSource {
    
    // Section = row to achieve the padding effect
    func numberOfSections(in tableView: UITableView) -> Int {
        return matches.count
    }
    
    // Always 1 row per section to achieve the desired padding effect
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    // Padding between rows
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(UIConfiguration.matchTableRowPadding)
    }
    
    // We don't care about the section header here, just make it transparent
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    // Prepare odd/even cells
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: MatchInitiableTableViewCell
        if indexPath.section % 2 == 0 {
            // Even
            cell = feedTableView.dequeueReusableCell(withIdentifier: UIConfiguration.leftMatchTableCellViewReusableId, for: indexPath) as! LeftMatchTableViewCell
        } else {
            // Odd
            cell = feedTableView.dequeueReusableCell(withIdentifier: UIConfiguration.rightMatchTableCellViewReusableId, for: indexPath) as! RightMatchTableViewCell
        }
        
        let match = matches[indexPath.section]
        match.isWatched = MatchCoreDataStorage.shared.isWatched(match: match)
        match.isBookmarked = MatchCoreDataStorage.shared.isBookmarked(match: match)
        cell.initFromMatch(match: match)
        
        return cell as! UITableViewCell
    }
    
    // Set alpha value for a cell just before it's displayed
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.white.withAlphaComponent(CGFloat(UIConfiguration.matchTableCellBackgroundAlpha))
        if indexPath.section == matches.count - 1 {
            // Load more matches when nearing the end of the list
            fetchMoreMatches(showProgressHud: false)
        }
    }
    
    // Play video & mark as watched upon row selection
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let match = matches[indexPath.section]
        
        // Mark as watched
        MatchCoreDataStorage.shared.saveAsWatched(match: match) {
            // Reload section if not yet watch to display watched icon
            reloadSection(section: indexPath.section)
        }
        
        // Play video
        playVideo(match.videoHandle!, seekTo: match.timestamp, errorHandler: handleVideoPlayerError)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let match = self.matches[indexPath.section]
        var bookmarkAction: UITableViewRowAction
        
        if MatchCoreDataStorage.shared.isBookmarked(match: match) {
            // Match is already bookmarked
            bookmarkAction = UITableViewRowAction(style: .normal, title: "Unbookmark") { (rowAction, indexPath) in
                MatchCoreDataStorage.shared.removeBookmark(match: match) {
                    self.reloadSection(section: indexPath.section)
                }
            }
        } else {
            // Match not yet bookmarked
            bookmarkAction = UITableViewRowAction(style: .normal, title: "Bookmark") { (rowAction, indexPath) in
                MatchCoreDataStorage.shared.saveAsBookmarked(match: match) {
                    self.reloadSection(section: indexPath.section)
                }
            }
        }
        bookmarkAction.backgroundColor = #colorLiteral(red: 0.6669999957, green: 0.5220000148, blue: 0.275000006, alpha: 1)
        return [bookmarkAction]
    }
    
    // Call rest service and attempt to feed more matches
    private func fetchMoreMatches(showProgressHud: Bool = true) {
        if !isMoreMatchesToFetch {
            // No more matches to load for the current filter
            return
        }
        
        // Assume no more matches before asynchronous fetch call, set true depending on result in completion handler
        isMoreMatchesToFetch = false
        matchFeedService.fetch(filter: feedFilter, showProgressHud: showProgressHud, completionHandler: handleMatchFetching, errorHandler: handleMatchFetchingError)
    }
    
    // Reset fetched data
    private func resetFetchedData() {
        isMoreMatchesToFetch = true
        feedFilter.page = RestConfiguration.firstPageIndex
        matches.removeAll()
        feedTableView.reloadData()
    }
    
    // Success completion handler for match fetching
    private func handleMatchFetching(fetchedMatches: [Match]) {
        if fetchedMatches.isEmpty {
            isMoreMatchesToFetch = false
        } else {
            // New matches loaded
            isMoreMatchesToFetch = true
            matches += fetchedMatches
            feedFilter.page += 1
            feedTableView.reloadData()
        }
    }
    
    /// Reload section to reflect model changes
    ///
    /// - Parameter section: section index
    private func reloadSection(section: Int) {
        let indexSet: IndexSet = [section]
        self.feedTableView.reloadSections(indexSet, with: .automatic)
    }
    
    private func handleVideoPlayerError(title: String, message: String) {
        showAlert(title: title, message: message)
    }
    
    // Error handler for match fetching
    private func handleMatchFetchingError(title: String, message: String) {
        isMoreMatchesToFetch = false
        showAlert(title: title, message: message)
    }

    /// Show alert
    ///
    /// - Parameters:
    ///   - title: alert title
    ///   - message: alert message
    ///   - handler: dismiss completion handler
    private func showAlert(title: String, message: String, handler: ((UIAlertAction) -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Keep on rocking", style: .default, handler: handler)
        alertController.addAction(dismissAction)
        present(alertController, animated: true, completion: nil)
    }
}

// Character select confirmation extension
extension FeedViewController : CharacterSelectConfirmable {
    func confirmCharacterSelect(firstCharacter: Character, secondCharacter: Character) {
        feedFilter.reset()
        
        // Update feed filter
        let anyCharacter = GameConfiguration.characters["any"]!
        if firstCharacter.id != anyCharacter.id {
            feedFilter.firstCharactedId = firstCharacter.id
        }
        
        if secondCharacter.id != anyCharacter.id {
            feedFilter.secondCharactedId = secondCharacter.id
        }
        
        // Apply
        refresh()
    }
    
    func deactivateBookmarkMode() {
        isBookmarkModeOn = false
        bookmarkButton.setImage(#imageLiteral(resourceName: "Bookmark.png"), for: .normal)
    }
    
    func deactivateCharacterSelect() {
        characterSelectButton.setImage(#imageLiteral(resourceName: "CharacterSelect.png"), for: .normal)
    }
}





