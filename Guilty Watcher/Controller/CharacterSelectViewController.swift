//
//  CharacterSelectViewController.swift
//  Guilty Watcher
//
//  Created by Petr Bludsky on 14/12/2017.
//  Copyright © 2017 Petr Bludsky. All rights reserved.
//

import UIKit

/// VC for character select presented as a modal window
class CharacterSelectViewController: UIViewController {
    
    /// First character id featured in the match (or any)
    var firstCharacter: Character?
    
    /// Character select delegate
    var delegate: CharacterSelectConfirmable?
    
    @IBOutlet weak var characterSelectScrollView: CharacterSelectScrollView!
    
    // Dismiss the modal window when touching outside
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        
        guard let location = touch?.location(in: self.view) else { return }
        if self.view.frame.contains(location) {
            // Set the menu button back to default right off the bat and not after closing animation is finished
            delegate?.deactivateCharacterSelect()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func characterButtonPressed(_ sender: UIButton) {
        // Draw circle around the button to indicate selection
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: 30,y: 30), radius: CGFloat(30), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        let shapeLayer = CAShapeLayer()
        
        shapeLayer.zPosition = -1
        shapeLayer.path = circlePath.cgPath
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = UIColor.red.cgColor
        shapeLayer.lineWidth = 7.0
        
        sender.layer.insertSublayer(shapeLayer, below: sender.layer)
        
        let characterId = sender.value(forKeyPath: "characterId") as! String
        let selectedCharacter = GameConfiguration.characters[characterId]!
        
        if let firstCharacterValue = firstCharacter {
            // Set the menu button back to default right off the bat and not after closing animation is finished
            delegate?.deactivateBookmarkMode()
            delegate?.deactivateCharacterSelect()
            // First character already selected, set filter and dismiss
            dismiss(animated: true, completion: {
                self.delegate?.confirmCharacterSelect(firstCharacter: firstCharacterValue, secondCharacter: selectedCharacter)
            })
        } else {
            // Setting first selected character
            firstCharacter = selectedCharacter
        }
    }

}
