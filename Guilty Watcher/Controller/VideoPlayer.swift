//
//  VideoPlayer.swift
//  Guilty Watcher
//
//  Created by Petr Bludsky on 06/11/2017.
//  Copyright © 2017 Petr Bludsky. All rights reserved.
//

import AVKit
import XCDYouTubeKit
import SVProgressHUD

// Handler type
typealias videoErrorHandler = (String, String) -> ()

/// Contains Youtube video quality constants
struct YouTubeVideoQuality {
    static let hd720 = NSNumber(value: XCDYouTubeVideoQuality.HD720.rawValue)
    static let medium360 = NSNumber(value: XCDYouTubeVideoQuality.medium360.rawValue)
    static let small240 = NSNumber(value: XCDYouTubeVideoQuality.small240.rawValue)
}

/// Video player extension of FeedViewController
extension FeedViewController {
    
    /// Play video from Youtube up to 720p quality
    ///
    /// - Parameter videoIdentifier: 11 char in length Youtube video indentifier
    /// - Parameter seekTo: time in seconds the video will be forwarded to
    func playVideo(_ videoIdentifier: String, seekTo: Int64, errorHandler: @escaping videoErrorHandler) {
        // Kill progress bar if any active
        SVProgressHUD.dismiss()
        
        // Present player
        self.present(playerViewController, animated: true, completion: nil)
        
        XCDYouTubeClient.default().getVideoWithIdentifier(videoIdentifier) { [weak playerViewController] (video: XCDYouTubeVideo?, error: Error?) in
            if let streamURLs = video?.streamURLs, let streamURL = (streamURLs[XCDYouTubeVideoQualityHTTPLiveStreaming] ?? streamURLs[YouTubeVideoQuality.hd720] ?? streamURLs[YouTubeVideoQuality.medium360] ?? streamURLs[YouTubeVideoQuality.small240]) {
                playerViewController?.player = AVPlayer(url: streamURL)
                // scale of 1 = first argument is always in seconds
                playerViewController?.player?.seek(to: CMTimeMake(seekTo, 1))
                playerViewController?.player?.play()
            } else {
                // Clear the buffer & clean up prior dismissal
                playerViewController?.player?.currentItem?.cancelPendingSeeks()
                playerViewController?.player?.currentItem?.asset.cancelLoading()
                self.dismiss(animated: true) {
                    errorHandler("Error playing match", "Video not available")
                }
            }
        }
    }
}
