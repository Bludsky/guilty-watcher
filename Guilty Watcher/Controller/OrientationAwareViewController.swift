//
//  OrientationAwareUIViewController.swift
//  Guilty Watcher
//
//  Created by Petr Bludsky on 30/10/2017.
//  Copyright © 2017 Petr Bludsky. All rights reserved.
//

import UIKit

/**
    Serves as a base for device orientation aware controllers enabling
    to automatically change their background depending on the current device orientation
    and supplying background bitmap differentiating between phone and tablet device type.

    Status bar is also changed to its white variant.
 */
class OrientationAwareViewController: UIViewController {
    
    /// Assign this in the descendant
    var background: UIImageView?
    
    /// If true blur effect will be in effect for bottom menu
    var hasBottomMenu: Bool = true
    
    /// Need viewDidAppear because of device orientation info
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let bounds = UIScreen.main.bounds
        updateBackground(to: CGSize(width: bounds.width, height: bounds.height))
    }
    
    // Upate view background upon orientation transition detection
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        updateBackground(to: size)
    }
    
    // Change status bar to white (light) variant
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // Update view background according to the current device orientation
    private func updateBackground(to size: CGSize) {
        var bgImage: String
        if UIDevice.current.orientation.isLandscape {
            // Landscape mode (and also upside down)
            bgImage = "LandscapeBackground"
        } else {
            // Portrait mode
            bgImage = "PortraitBackground"
        }
        
        background?.image = UIImage(named: bgImage)
        
        if (hasBottomMenu) {
            // Apply blur effect for bottom menu
            background?.removeBlurEffect()
            background?.addBlurEffect(to: size)
        }
    }
    
}

/// Dynamic blur effect extension
extension UIImageView {
    func addBlurEffect(to size: CGSize) {
        let blurEffectView = UIVisualEffectView(
            effect: UIBlurEffect(style: .prominent)
        )
        
        let effectViewHeight = size.height * CGFloat(UIConfiguration.bottomMenuHeightRatio)
        blurEffectView.frame = CGRect(x: 0, y: size.height - effectViewHeight,
                                      width: size.width, height: effectViewHeight)
        
        self.addSubview(blurEffectView)
    }
    
    func removeBlurEffect() {
        for subview in self.subviews {
            if subview is UIVisualEffectView {
                subview.removeFromSuperview()
            }
        }
    }
}

