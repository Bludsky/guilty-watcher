//
//  CharacterSelectScrollView.swift
//  Guilty Watcher
//
//  Created by Petr Bludsky on 14/12/2017.
//  Copyright © 2017 Petr Bludsky. All rights reserved.
//

import UIKit

/// Scrollable character select window
@IBDesignable
class CharacterSelectScrollView: UIScrollView {

    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
}
