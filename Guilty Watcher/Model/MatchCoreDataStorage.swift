//
//  MatchCoreDataStorage.swift
//  Guilty Watcher
//
//  Created by Petr Bludsky on 30/11/2017.
//  Copyright © 2017 Petr Bludsky. All rights reserved.
//

import UIKit

typealias storageCompletionHandler = (() -> Void)

/// Stores watched & bookmarked matches
protocol MatchDataStorage {
    
    /// Persist match as bookmarked
    ///
    /// - Parameters:
    ///   - match: match to be persisted
    ///   - handler: completion handler (usually reload ui)
    func saveAsWatched(match: Match, handler: storageCompletionHandler)
    
    /// Persist match as bookmarked
    ///
    /// - Parameters:
    ///   - match: match to be persisted
    ///   - handler: completion handler (usually reload ui)
    func saveAsBookmarked(match: Match, handler: storageCompletionHandler)
    
    /// Remove bookmark (match remains as watched)
    ///
    /// - Parameters:
    ///   - match: match to unbookmark
    ///   - handler: completion handler (usually reload ui)
    func removeBookmark(match: Match, handler: storageCompletionHandler)
    
    /// Find whether match has been already watched
    ///
    /// - Parameter match: match to check
    /// - Returns: true if watched otherwise false
    func isWatched(match: Match) -> Bool
    
    /// Find whether match is currently bookmarked
    ///
    /// - Parameter match: match to check
    /// - Returns: true if currently bookmarked otherwise false
    func isBookmarked(match: Match) -> Bool
    
    /// Fetch all stored bookmarks
    ///
    /// - Returns: array of bookmarked matches
    func fetchBookmarks() -> [Match]
    
    /// Persist entities in the context
    func saveContext()
}

/// CoreData implementation of MatchDataStorage
final class MatchCoreDataStorage: MatchDataStorage {
    
    /// Shared instance of MatchCoreDataStorage
    static let shared: MatchDataStorage = MatchCoreDataStorage()
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    private var storedMatches = [Match]()
    
    private init() {
        // TODO storedMatches init
        do {
            storedMatches = try context.fetch(Match.fetchRequest()) as [Match]
        } catch {
            // Fail quietly
            print(error)
        }
    }
    
    func saveAsWatched(match: Match, handler: storageCompletionHandler) {
        match.isWatched = true
        if let storedMatch = findStoredMatch(match: match) {
            // Match already persisted
            storedMatch.isWatched = true
        } else {
            storedMatches.append(convertToManagedEntity(match: match))
        }
        handler()
    }
    
    func saveAsBookmarked(match: Match, handler: storageCompletionHandler) {
        match.isBookmarked = true
        if let storedMatch = findStoredMatch(match: match) {
            // Match already persisted
            storedMatch.isBookmarked = true
        } else {
            storedMatches.append(convertToManagedEntity(match: match))
        }
        handler()
    }
    
    func removeBookmark(match: Match, handler: storageCompletionHandler) {
        match.isBookmarked = false
        if let storedMatch = findStoredMatch(match: match) {
            storedMatch.isBookmarked = false
            handler()
        }
    }
    
    func isWatched(match: Match) -> Bool {
        if let storedMatch = findStoredMatch(match: match) {
            return storedMatch.isWatched
        }
        return false
    }
    
    func isBookmarked(match: Match) -> Bool {
        if let storedMatch = findStoredMatch(match: match) {
            return storedMatch.isBookmarked
        }
        return false
    }
    
    func fetchBookmarks() -> [Match] {
        return storedMatches.filter({ (storedMatch) -> Bool in
            return storedMatch.isBookmarked
        })
    }
    
    func saveContext() {
        do {
            if context.hasChanges {
                try context.save()
            }
        } catch {
            // Fail quietly
            print(error)
        }
    }
    
    private func findStoredMatch(match: Match) -> Match? {
        let filteredMatches = storedMatches.filter { (storedMatch) -> Bool in
            return storedMatch.matchId == match.matchId
        }
        if filteredMatches.count == 0 {
            return nil
        } else if (filteredMatches.count == 1) {
            return filteredMatches[0]
        } else {
            fatalError("Multiple matches sharing same match id detected")
        }
    }
    
    // POSO -> managed entity
    private func convertToManagedEntity(match: Match) -> Match {
        return Match(context: context, saveEntity: true, videoHandle: match.videoHandle!, timestamp: match.timestamp, date: match.date!, title: match.title!, firstCharacter: match.firstCharacter, secondCharacter: match.secondCharacter, firstPlayer: match.firstPlayer!, secondPlayer: match.secondPlayer!, isWatched: match.isWatched, isBookmarked: match.isBookmarked)
    }
}
