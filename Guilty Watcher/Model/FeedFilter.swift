//
//  FeedFilter.swift
//  Guilty Watcher
//
//  Created by Petr Bludsky on 06/11/2017.
//  Copyright © 2017 Petr Bludsky. All rights reserved.
//

/// Contains querying parameters for MatchFeedService
class FeedFilter {
    
    /// 1 page = 10 matches, non-existing page returns 0 matches
    var page: Int = RestConfiguration.firstPageIndex
    
    /// Optional first character id featured in the match
    var firstCharactedId: String?
    
    /// Optional second character id featured in the match
    var secondCharactedId: String?
    
    convenience init(page: Int) {
        self.init()
        self.page = page
    }
    
    /// Reset the filter to its default state
    func reset() {
        page = RestConfiguration.firstPageIndex
        firstCharactedId = nil
        secondCharactedId = nil
    }
    
}
