//
//  MatchFeedService.swift
//  Guilty Watcher
//
//  Created by Petr Bludsky on 30/10/2017.
//  Copyright © 2017 Petr Bludsky. All rights reserved.
//
import Foundation
import Alamofire
import SwiftyJSON
import SVProgressHUD

// Handler types
typealias restCompletionHandler = ([Match]) -> ()
typealias restErrorHandler = (String, String) -> ()

/// Match feed service responsible for loading matches from source
protocol MatchFeedService {
    
    /// Query rest endpoint for matches
    ///
    /// - Parameters:
    ///   - filter: contains query data for feed service
    ///   - showProgressHud: show progress hud while servicing the request if true, otherwise do nothing
    func fetch(filter: FeedFilter, showProgressHud: Bool, completionHandler: @escaping restCompletionHandler, errorHandler: @escaping restErrorHandler)
}

/// Rest implementation of MatchFeedService
class RestMatchFeedService : MatchFeedService {
    
    // Convert string date (e.g. 2017-11-13) to string date
    let formatter = DateFormatter()
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    init() {
        formatter.dateFormat = UIConfiguration.dateFormat
    }
    
    func fetch(filter: FeedFilter, showProgressHud: Bool, completionHandler: @escaping restCompletionHandler, errorHandler: @escaping restErrorHandler) {
        if showProgressHud {
            SVProgressHUD.show()
        }
        
        // Filter -> parameters
        var params = ["page": String(filter.page)]
        if let firstCharacterId = filter.firstCharactedId {
            params.updateValue(firstCharacterId, forKey: "p1chars")
        }
        if let secondCharacterId = filter.secondCharactedId {
            params.updateValue(secondCharacterId, forKey: "p2chars")
        }
    
        Alamofire.request(RestConfiguration.sourceUrl, method: .get, parameters: params).responseJSON { (response) in
            if response.result.isSuccess {
                completionHandler(self.parseJson(json: JSON(response.result.value!)))
            } else {
                // Error occurred
                errorHandler("Error fetching matches", "Network endpoint not available")
            }
            defer {
                SVProgressHUD.dismiss()
            }
        }
    }

    // Parse Json to array of Match
    func parseJson(json: JSON) -> [Match] {
        var matches = [Match]()
        for (_, matchJson):(String, JSON) in json {
            
            // Video handle
            let videoHandle = matchJson["video"].stringValue
            if videoHandle.count != 11 {
                continue
            }
            
            // Date
            let dateString = matchJson["date"].stringValue
            if dateString == "" {
                continue
            }
            let date = formatter.date(from: dateString)
            
            // Title
            let title = matchJson["title"].stringValue
            
            // Timestamp
            let timestamp = matchJson["timestamp"].stringValue
            if timestamp == "" {
                continue
            }
            
            // Characters
            let firstCharacterId = matchJson["players"][0]["characters"][0]["id"].stringValue
            let secondCharacterId = matchJson["players"][1]["characters"][0]["id"].stringValue
            if firstCharacterId == "" || secondCharacterId == "" {
                continue
            }
            
            // Player names (optional, blank string valid)
            let firstPlayer = matchJson["players"][0]["name"].stringValue
            let secondPlayer = matchJson["players"][1]["name"].stringValue
            
            if let dateValue = date, let firstCharacter = GameConfiguration.characters[firstCharacterId], let secondCharacter = GameConfiguration.characters[secondCharacterId]  {
                let match = Match(context: context, videoHandle: videoHandle, timestamp: parseTimestampToSeconds(timestamp: timestamp), date: dateValue, title: title, firstCharacter: firstCharacter, secondCharacter: secondCharacter, firstPlayer: firstPlayer, secondPlayer: secondPlayer)
                matches.append(match)
            }
        }
        return matches
    }
    
    // Parse string timestamp (e.g. 03h42m20s) to number of seconds
    func parseTimestampToSeconds(timestamp: String) -> Int64 {
        let res = matches(for: "[0-9]{2}", in: timestamp)
        if res.count != 3 {
            // Mangled timestamp, seek to 0
            return 0
        } else {
            return Int64(res[0])! * 3600 + Int64(res[1])! * 60 + Int64(res[2])!
        }
    }
    
    // Searches text for occurrences of regex pattern
    func matches(for regex: String, in text: String) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: text,
                                        range: NSRange(text.startIndex..., in: text))
            return results.map {
                String(text[Range($0.range, in: text)!])
            }
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
}
