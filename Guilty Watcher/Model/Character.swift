//
//  Character.swift
//  Guilty Watcher
//
//  Created by Petr Bludsky on 04/11/2017.
//  Copyright © 2017 Petr Bludsky. All rights reserved.
//

import Foundation

/// Guilty Gear character entity
class Character {
    
    /// Character handle (e.g. millia)
    let id: String
    
    /// Character name (e.g. Millia Rage)
    let name: String
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
    
}
