//
//  Configuration.swift
//  Guilty Watcher
//
//  Created by Petr Bludsky on 30/10/2017.
//  Copyright © 2017 Petr Bludsky. All rights reserved.
//

import Foundation

// MARK: Rest configuration segment
/// Rest service configuration
enum RestConfiguration {
    /// Match feed source url
    static let sourceUrl = "https://us-central1-keeponrock-in.cloudfunctions.net/app/matches"
    /// First page index
    static let firstPageIndex = 1
}

// MARK: UI Configuration segment
/// UI configuration
enum UIConfiguration {
    /// Height ratio of bottom menu to top view
    static let bottomMenuHeightRatio = 0.12
    
    /// Alpha value of match table cell background
    static let matchTableCellBackgroundAlpha = 0.6
    
    /// Left match table cell reusable id
    static let leftMatchTableCellViewReusableId = "leftMatchTableCellView"
    
    /// Right match table cell reusable id
    static let rightMatchTableCellViewReusableId = "rightMatchTableCellView"
    
    /// Padding between rows
    static let matchTableRowPadding = 10
    
    /// Segue
    static let goToVideoPlayerSegue = "goToVideoPlayer"
    
    /// Date
    static let dateFormat = "YYYY-MM-dd"
}

// MARK: Game Configuration segment
/// Guilty Gear related configuration
enum GameConfiguration {
    static let characters = [
        "any": Character(id: "any", name: "Any"),
        "answer": Character(id: "answer", name: "Answer"),
        "axl": Character(id: "axl", name: "Axl Low"),
        "baiken": Character(id: "baiken", name: "Baiken"),
        "bedman": Character(id: "bedman", name: "Bedman"),
        "chipp": Character(id: "chipp", name: "Chipp Zanuff"),
        "dizzy": Character(id: "dizzy", name: "Dizzy"),
        "elphelt": Character(id: "elphelt", name: "Elphelt Valentine"),
        "faust": Character(id: "faust", name: "Faust"),
        "ino": Character(id: "ino", name: "I-No"),
        "jack": Character(id: "jack", name: "Jack-O' Valentine"),
        "jam": Character(id: "jam", name: "Jam Kuradoberi"),
        "johnny": Character(id: "johnny", name: "Johnny"),
        "kum": Character(id: "kum", name: "Kum Haehyun"),
        "ky": Character(id: "ky", name: "Ky Kiske"),
        "leo": Character(id: "leo", name: "Leo Whitefang"),
        "may": Character(id: "may", name: "May"),
        "millia": Character(id: "millia", name: "Millia Rage"),
        "potemkin": Character(id: "potemkin", name: "Potemkin"),
        "ramlethal": Character(id: "ramlethal", name: "Ramlethal Valentine"),
        "raven": Character(id: "raven", name: "Raven"),
        "sin": Character(id: "sin", name: "Sin Kiske"),
        "slayer": Character(id: "slayer", name: "Slayer"),
        "sol": Character(id: "sol", name: "Sol Badguy"),
        "venom": Character(id: "venom", name: "Venom"),
        "zato": Character(id: "zato", name: "Zato-1")
    ]
}
