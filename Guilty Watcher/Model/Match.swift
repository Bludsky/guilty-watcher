//
//  Match.swift
//  Guilty Watcher
//
//  Created by Petr Bludsky on 04/11/2017.
//  Copyright © 2017 Petr Bludsky. All rights reserved.
//

import Foundation
import CoreData

/// Match entity
extension Match {
    
    /// First character of the match
    var firstCharacter: Character {
        get {
            willAccessValue(forKey: "firstCharacter")
            defer { didAccessValue(forKey: "firstCharacter") }
            return GameConfiguration.characters[firstCharacterId!]!
        }
        set {
            willChangeValue(forKey: "firstCharacter")
            defer { didChangeValue(forKey: "firstCharacter") }
            firstCharacterId = firstCharacter.id
        }
    }
    
    /// Second character of the match
    var secondCharacter: Character {
        get {
            willAccessValue(forKey: "secondCharacter")
            defer { didAccessValue(forKey: "secondCharacter") }
            return GameConfiguration.characters[secondCharacterId!]!
        }
        set {
            willChangeValue(forKey: "secondCharacter")
            defer { didChangeValue(forKey: "secondCharacter") }
            secondCharacterId = secondCharacter.id
        }
    }
    
    /// Transient match id
    var matchId: String {
        get {
            return videoHandle! + String(timestamp)
        }
    }
    
    convenience init(context: NSManagedObjectContext, saveEntity: Bool = false, videoHandle: String, timestamp: Int64, date: Date, title: String, firstCharacter: Character, secondCharacter: Character, firstPlayer: String, secondPlayer: String, isWatched: Bool = false, isBookmarked: Bool = false) {
        let desc = NSEntityDescription.entity(forEntityName: "Match", in: context)
        self.init(entity: desc!, insertInto: (saveEntity) ? context: nil)
        self.videoHandle = videoHandle
        self.timestamp = timestamp
        self.date = date
        self.title = title
        self.firstCharacterId = firstCharacter.id
        self.secondCharacterId = secondCharacter.id
        self.firstPlayer = firstPlayer
        self.secondPlayer = secondPlayer
        self.isWatched = isWatched
        self.isBookmarked = isBookmarked
    }
    
}
