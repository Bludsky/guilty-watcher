//
//  MatchTableViewCell.swift
//  Guilty Watcher
//
//  Created by Petr Bludsky on 06/11/2017.
//  Copyright © 2017 Petr Bludsky. All rights reserved.
//

import UIKit

class MatchTableViewCell {
    
    /// Set highlighted status for match cell
    ///
    /// - Parameters:
    ///   - cell: match cell
    ///   - highlighted: is cell highlighted
    ///   - labels: labels which style get changed
    static func setHighlighted(cell: UITableViewCell, highlighted: Bool, match: Match, icon: UIImageView, labels: UILabel...) {
        let color = (highlighted) ? UIColor.black : UIColor.white
        let labelColor = (highlighted) ? UIColor.white : #colorLiteral(red: 0.4199557602, green: 0.4200309217, blue: 0.4199458957, alpha: 1)
        cell.backgroundColor = color.withAlphaComponent(CGFloat(UIConfiguration.matchTableCellBackgroundAlpha))
        
        for label in labels {
            label.textColor = labelColor
        }
        
        // Icon
        if match.isBookmarked {
            icon.image = (highlighted) ? #imageLiteral(resourceName: "RowBookmarkActive.png"): #imageLiteral(resourceName: "RowBookmark.png")
        } else if match.isWatched {
            icon.image = (highlighted) ? #imageLiteral(resourceName: "RowEyeActive.png"): #imageLiteral(resourceName: "RowEye.png")
        } else {
            icon.image = nil
        }
    }
    
}
