//
//  LeftMatchTableViewCell.swift
//  Guilty Watcher
//
//  Created by Petr Bludsky on 01/11/2017.
//  Copyright © 2017 Petr Bludsky. All rights reserved.
//

import UIKit
import THLabel

protocol MatchInitiableTableViewCell {
    
    /// Initialize labels/images form match
    ///
    /// - Parameter match: match containing data
    func initFromMatch(match: Match)
}

/// Left match cell for odd/even styling
/// FIXME code duplicity with RightMatchTableViewCell
class LeftMatchTableViewCell: UITableViewCell, MatchInitiableTableViewCell {
    
    private var match: Match?

    // MARK: views
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var rightView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    
    // MARK: imageViews
    @IBOutlet weak var firstCharacterImageView: UIImageView!
    @IBOutlet weak var secondCharacterImageView: UIImageView!
    
    // MARK: labels
    @IBOutlet weak var vsLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet var firstPlayerLabel: THLabel!
    @IBOutlet var secondPlayerLabel: THLabel!
    
    private let formatter = DateFormatter()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        selectionStyle = UITableViewCellSelectionStyle.none
        leftView.backgroundColor = UIColor.clear
        rightView.backgroundColor = UIColor.clear
        
        formatter.locale = Locale.current
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        // Same as highlighted here
        // Selection is reset upon dismissing the video player
        setHighlighted(selected, animated: animated)
    }
    
    // Override cell selection style
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        MatchTableViewCell.setHighlighted(cell: self, highlighted: highlighted, match: match!, icon: iconImageView, labels: vsLabel, dateLabel, descriptionLabel)
    }
    
    func initFromMatch(match: Match) {
        self.match = match
        dateLabel.text = formatter.string(from: match.date!)
        descriptionLabel.text = match.title
        firstCharacterImageView.image = UIImage(named: match.firstCharacter.id)
        secondCharacterImageView.image = UIImage(named: match.secondCharacter.id)
        firstPlayerLabel.text = match.firstPlayer
        secondPlayerLabel.text = match.secondPlayer
        MatchTableViewCell.setHighlighted(cell: self, highlighted: false, match: match, icon: iconImageView, labels: vsLabel, dateLabel, descriptionLabel)
    }
}
